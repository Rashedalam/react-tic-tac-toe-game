import React, {Component} from 'react'
import './App.css';
import Game from "./components/Game";

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <h1 style={{textAlign: 'center'}}>TIC TAC GAME</h1>
                <Game/>
            </React.Fragment>
        )
    }
}

export default App;