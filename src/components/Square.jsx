import React from "react";

function renderValue(value) {
    if (value === null)
    {
        return(
            <span>&nbsp;</span>
        )
    }else{
        return value
    }
}

function Square(props) {
    return (
        <button style={{width: '40px', height: '40px', border: '1px solid #000'}} onClick={props.onClick}>
            {renderValue(props.value)}
        </button>
    )
}

export default Square