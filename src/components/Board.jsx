import React, {Component} from 'react';
import Square from "./Square";


class Board extends Component {

    square = (i) => {
        return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)}/>
    };

    render() {
        return (
            <div style={{textAlign: 'center', margin: "0 auto"}}>
                <div>
                    {this.square(0)}
                    {this.square(1)}
                    {this.square(2)}
                </div>
                <div>
                    {this.square(3)}
                    {this.square(4)}
                    {this.square(5)}
                </div>
                <div>
                    {this.square(6)}
                    {this.square(7)}
                    {this.square(8)}
                </div>
            </div>
        )
    }
}

export default Board